<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\clientes;
use Faker\Generator as Faker;

$factory->define(clientes::class, function (Faker $faker) {

    return [
        'nome' => $faker->word,
        'cpf' => $faker->word,
        'email' => $faker->word,
        'telefone' => $faker->word,
        'cep' => $faker->word,
        'logradouro' => $faker->word,
        'cidade' => $faker->word,
        'bairro' => $faker->word,
        'uf' => $faker->word,
        'numero' => $faker->word,
        'complemento' => $faker->word,
        'remember_token' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
