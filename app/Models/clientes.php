<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class clientes
 * @package App\Models
 * @version February 5, 2020, 5:33 pm UTC
 *
 * @property string nome
 * @property string cpf
 * @property string email
 * @property string telefone
 * @property string cep
 * @property string logradouro
 * @property string cidade
 * @property string bairro
 * @property string uf
 * @property string numero
 * @property string complemento
 * @property string remember_token
 */
class clientes extends Model
{
    use SoftDeletes;

    public $table = 'clientes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nome',
        'cpf',
        'email',
        'telefone',
        'cep',
        'logradouro',
        'cidade',
        'bairro',
        'uf',
        'numero',
        'complemento',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'cpf' => 'string',
        'email' => 'string',
        'telefone' => 'string',
        'cep' => 'string',
        'logradouro' => 'string',
        'cidade' => 'string',
        'bairro' => 'string',
        'uf' => 'string',
        'numero' => 'string',
        'complemento' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'cpf' => 'required',
        'email' => 'required',
        'telefone' => 'required',
        'cep' => 'required',
        'logradouro' => 'required',
        'cidade' => 'required',
        'bairro' => 'required',
        'uf' => 'required',
        'numero' => 'required',
        'complemento' => 'required'
    ];

    
}
