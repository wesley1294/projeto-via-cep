<div class="table-responsive">
    <table class="table" id="clientes-table">
        <thead>
            <tr>
                <th>Nome</th>
        <th>Cpf</th>
        <th>Email</th>
        <th>Telefone</th>
        <th>Cep</th>
        <th>Logradouro</th>
        <th>Cidade</th>
        <th>Bairro</th>
        <th>Uf</th>
        <th>Numero</th>
        <th>Complemento</th>
        <th>Remember Token</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($clientes as $clientes)
            <tr>
                <td>{{ $clientes->nome }}</td>
            <td>{{ $clientes->cpf }}</td>
            <td>{{ $clientes->email }}</td>
            <td>{{ $clientes->telefone }}</td>
            <td>{{ $clientes->cep }}</td>
            <td>{{ $clientes->logradouro }}</td>
            <td>{{ $clientes->cidade }}</td>
            <td>{{ $clientes->bairro }}</td>
            <td>{{ $clientes->uf }}</td>
            <td>{{ $clientes->numero }}</td>
            <td>{{ $clientes->complemento }}</td>
            <td>{{ $clientes->remember_token }}</td>
                <td>
                    {!! Form::open(['route' => ['clientes.destroy', $clientes->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('clientes.show', [$clientes->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('clientes.edit', [$clientes->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
